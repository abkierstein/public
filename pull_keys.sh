#!/usr/bin/env bash

# This script should be used to update authorized keys from git source

function usage {
    echo "This utility will dump url output to authorized_keys..."
    echo "Usage: pull_keys.sh -u https://gitlab.com/abkierstein/public/raw/master/authorized_keys" 
}

while getopts ":u:h" opt; do
    case ${opt} in
        u ) # Specify a URL
            url=$OPTARG
        ;;
        h ) usage && exit 1
        ;;
        \? ) usage && exit 1
        ;;
    esac
done

# Handle empty args
if [ $OPTIND -eq 1 ]; then
    usage
    exit 1
fi

# Require URL must be set
if [ "x" == "x$url" ]; then
    echo "ERROR: A URL must be set" >&2
    usage
    exit 1
fi

# Check ssh dir
if [ ! -d "$HOME/.ssh" ]; then
    mkdir "$HOME/.ssh"
fi

# protect current version of authorized keys
if [ -f "$HOME/.ssh/authorized_keys" ]; then
    echo "Authorized keys file exists, making a backup..."
    cp $HOME/.ssh/authorized_keys $HOME/.ssh/authorized_keys.bkp
fi

# Pull keys and place them in .ssh
echo "Curling: $url"
curl $url -o "$HOME/.ssh/authorized_keys"

if [ -f "$HOME/.ssh/authorized_keys.bkp" ]; then
    echo "Old authorized keys content..."
    cat "$HOME/.ssh/authorized_keys.bkp"
fi

echo "Updated authorized keys content..."
cat "$HOME/.ssh/authorized_keys"
